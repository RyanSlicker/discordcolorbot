const Discord = require('discord.js');
require('dotenv').load();

const config = require('./config.json');
config.color_api_key = process.env.COLOR_API_KEY;

const functions = require('./app/functions.js')(process.env);
const commands = require('./app/commands.js')(Discord, config, functions);

let firstSetup = true;
let currentRoles = {};

(function setupClient() {
    const client = new Discord.Client();

    client.on('ready', () => {
        functions.log('Ready!', 'success');

        if (config.activity.length > 0) {
            client.user.setActivity(config.activity);
        }

        if (firstSetup) {
            if (config.cycle_roles) {
                client.guilds.array().forEach(function (guild) {
                    const roles = functions.getAssignableRoles(guild.roles);

                    if (roles.length >= 2) {
                        functions.replaceMemberRoles(guild.me, roles, roles[0]);
                    }
                });
            }
        }
        firstSetup = false;
    });

    client.on('message', message => {
        if (message.content.startsWith(config.prefix)) {
            handleCommand(message);
        }
    });

    if (config.cycle_roles) {
        client.setInterval(function () { // Change role to next color every 2 hours
            client.guilds.array().forEach(function (guild) {
                const roles = functions.getAssignableRoles(guild.roles);

                if (roles.length >= 2) {
                    if (typeof currentRoles[guild.id] == 'undefined') {
                        currentRoles[guild.id] = 0;
                    }

                    const currentRole = currentRoles[guild.id];
                    const nextRole = (currentRole + 1) < roles.length ? (currentRole + 1) : 0;
                    currentRoles[guild.id] = nextRole;

                    functions.replaceMemberRole(guild.me, roles[currentRole], roles[nextRole]);
                }
            });
        }, (2 * 60 * 60 * 1000));
    }

    client.setTimeout(function () { // Restart client every 12 hours
        functions.log('Restarting...', 'warning');
        client.destroy().then(function() {
            setupClient();
        });
    }, ((12 * 60 * 60 * 1000) + 30));

    client.on("error", (e) => console.log(e));
    client.on("warn", (e) => console.log(e));

    client.login(process.env.DISCORD_TOKEN).then();
})();

function handleCommand(message) {
    const input = message.content.toLowerCase().substr(config.prefix.length);

    if (input.length > 0) {
        const parts = input.split(' ');

        const command = parts[0];
        let parameters = [];

        if (parts.length > 1) {
            parts.shift();
            parameters = parts;
        }

        if (typeof commands[command] !== 'undefined') {
            functions.debug('Running ' + command + ' command.', 'info', message.member);

            if ((commands[command].execute.length - 1) > parameters.length) {
                functions.debug('Command parameters missing.', 'warning', message.member);
                message.channel.send("Parameters missing.");
            } else {
                commands[command].execute(message, parameters);
            }
        }
    }
}